package HomeWork2Version2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TextReader {

    public String readTxt(String s) throws IOException {
        String line;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(s))) {
            line = bufferedReader.readLine();
        }
        return line;
    }

}
