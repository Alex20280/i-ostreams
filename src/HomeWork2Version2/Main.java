package HomeWork2Version2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

//Hello_World.jar
//HelloTXTWorld.txt

public class Main extends JFrame {

    public static void main(String[] args) {

        GraphicComponent frame = new GraphicComponent();
        JarReader jarReader = new JarReader();
        TextReader textReader = new TextReader();

        String jar = "jar";
        String txt = "txt";

        EventQueue.invokeLater(() -> {
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);

            frame.insertButton.addActionListener((ActionEvent l) -> {
                String text = frame.textField.getText();
                if (checker(text).equals(jar)) {
                    try {
                        String s = jarReader.readJar(text);
                        frame.printJar(s);

                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                } else if (checker(text).equals(txt)) {
                    try {
                        String txtResult = textReader.readTxt(text);
                        frame.printTxt(txtResult);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        });


    }

    public static String checker(String string) {
        File file = new File(string);
        String filename = file.getName();
        return string.substring(filename.lastIndexOf(".") + 1, filename.length());
    }


}
