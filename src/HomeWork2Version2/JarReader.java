package HomeWork2Version2;

import java.io.*;


public class JarReader {

        public String readJar(String s) throws IOException, InterruptedException {
                Process ps = Runtime.getRuntime().exec(new String[]{"java","-jar", s});
                ps.waitFor();
                java.io.InputStream is=ps.getInputStream();
                byte b[]=new byte[is.available()];
                is.read(b,0,b.length);
                return new String(b);
        }
}

