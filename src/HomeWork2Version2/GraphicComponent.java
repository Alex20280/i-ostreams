package HomeWork2Version2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class GraphicComponent extends JFrame {

    public static final int DEFAULT_WIDTH = 500;
    public static final int DEFAULT_HEIGHT = 300;

    JTextArea textArea;
    JTextField textField;
    JScrollPane scrollPane;
    JPanel southPanel;
    JButton insertButton;

    JarReader jarReader = new JarReader();

    public GraphicComponent() {
        setTitle("Input/Output");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        textArea = new JTextArea(8, 40);

        //TextField
        textField = new JTextField();
        JPanel northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(2, 2));
        northPanel.add(new JLabel("Input the full name of the file: ", SwingConstants.LEFT));
        northPanel.add(textField);
        add(northPanel, BorderLayout.NORTH);

        //TextArea
        scrollPane = new JScrollPane(textArea);
        southPanel = new JPanel();
        add(scrollPane, BorderLayout.CENTER);

        //Button
        insertButton = new JButton("Check");
        southPanel.add(insertButton);
        add(southPanel, BorderLayout.SOUTH);

    }

    public void printJar(String text) {
        textArea.setText("Jar file executed. Its content: " + text);
    }

    public void printTxt(String text) {
        textArea.setText("Txt file executed. Its content: " + text);
    }

}