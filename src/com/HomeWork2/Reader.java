package com.HomeWork2;

import java.io.IOException;

public interface  Reader {

    void readJar(String s) throws IOException, InterruptedException;

    void readTxt(String s) throws IOException, InterruptedException;
}
