package com.HomeWork2;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

//HelloWorld.jar
//HelloTXTWorld.txt

public class Main {

    static String jar = "jar";
    static String txt = "txt";

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("Input the full name of the file: ");
        Scanner scanner = new Scanner(System.in);
        String scan = scanner.nextLine();

        Executor executor = new Executor();

        if (checker(scan).equals(jar)) {
            executor.readJar(scan);
        } else if (checker(scan).equals(txt)){
            executor.readTxt(scan);
        }
        scanner.close();
    }

    public static String checker(String string) {
        File file = new File(string);
        String filename = file.getName();
        return string.substring(filename.lastIndexOf(".") + 1, filename.length());
    }
}
