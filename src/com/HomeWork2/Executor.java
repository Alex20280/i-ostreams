package com.HomeWork2;

import java.io.*;

public class Executor implements Reader {

    @Override
    public void readJar(String s) throws IOException, InterruptedException {
            Process ps = Runtime.getRuntime().exec(new String[]{"java","-jar", s});
            ps.waitFor();
            java.io.InputStream is=ps.getInputStream();
            byte b[]=new byte[is.available()];
            is.read(b,0,b.length);
            System.out.println("Jar file executed. Its content: ");
            System.out.println(new String(b));
    }

    @Override
    public void readTxt(String s) throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(s))) {
            String buf;
            System.out.println("Txt file executed. Its content: ");
            while ((buf = bufferedReader.readLine()) != null) {
                System.out.println(buf);
            }
        }
    }

}
